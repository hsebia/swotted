"""
SWoTTeD Module

@author: Hana Sebia and Thomas Guyet
@date: 2023
@institution: Inria
"""

from swotted.swotted import swottedModule, swottedTrainer
from swotted.fastswotted import (
    fastSWoTTeDDataset,
    fastSWoTTeDModule,
    fastSWoTTeDTrainer,
)


name = "swotted"
