---
title: TODO
---

# SWoTTeD model improvements

* auto-SWoTTeD: integrate the automatic selection of (some) optimal parameters through a validation step. The idea would be also to start with different random initial states and the run few steps to select one of them on a validation dataset. The choosen model would be refined with more steps.
* hybrid-SWoTTeD: an implementation of a tensor decomposition method that handles features with different characteristics (and different metrics in losses)

# Examples

* Create more example in the documentation

# Meta

* Make some more experiments/improvements for GPU

