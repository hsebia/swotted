.. SWoTTeD documentation master file

``SWoTTeD``'s documentation
==========================

.. raw:: html

      <center>
         <img src="sliding_window_temporal_phenotyping.png">
      </center>


``SWoTTeD`` is a Python package that provides an implementation of the _SWoTTeD_
machine learning model. The _SWoTTeD_ model is an extension of tensor decomposition
to temporal data. It extracts temporal patterns from the a collection of temporal
tensors. 

This package builds on ``pytorch`` and ``pytorch-lightning`` libraries. This makes 
this model usable in your own machine learning architectures.


.. toctree::
   :maxdepth: 2
   :glob:

   readme
   notebooks/SWoTTeD_firstrun
   notebooks/SWoTTeD_module_example
   modules
   citing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
