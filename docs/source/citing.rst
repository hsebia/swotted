Citing ``SWoTTeD``
===================

If you use ``SWoTTeD`` in a scientific publication, we would appreciate citations:

  Bibtex entry::

    @misc{Sebia23_SWoTTeD_ArXiV,
          title         = {SWoTTeD: An Extension of Tensor Decomposition to Temporal Phenotyping}, 
          author        = {Hana Sebia and Thomas Guyet and Etienne Audureau},
          year          = {2023},
          eprint        = {2310.01201},
          archivePrefix = {arXiv},
          url           = {https://arxiv.org/abs/2310.01201}
    }

  Bibtex entry::

    @inproceedings{Sebia23_SWoTTeD,
        author       = {Hana Sebia and Thomas Guyet and Etienne Audureau},
        title        = {Une extension de la d{\'{e}}composition tensorielle au ph{\'{e}}notypage temporel},
        booktitle    = {Actes de la conf{\'e}rence Extraction et Gestion des Connaissances (EGC)},
        volume       = {{E-39}},
        pages        = {43--54},
        publisher    = {Editions {RNTI}},
        year         = {2023},
        url          = {http://editions-rnti.fr/?inprocid=1002809}
    }